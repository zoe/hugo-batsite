import { updateMode } from "./lightmode.js";
import { activateHamburger } from "./hamburger.js";
import { randomizeWords } from "./randomword.js";
import { unhide_elements } from "./noscript.js";

document.addEventListener("DOMContentLoaded", () => {
  updateMode();
  activateHamburger();
  randomizeWords();
  unhide_elements();
});
