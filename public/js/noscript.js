const flexes = document.getElementsByClassName("flexjs");
const iflexes = document.getElementsByClassName("iflexjs");
export function unhide_elements() {
  for (let element of flexes) {
    element.style.display = "flex";
  }
  for (let element of iflexes) {
    element.style.display = "inline-flex";
  }
}
