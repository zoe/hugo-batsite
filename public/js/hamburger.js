const button = document.getElementById("hamburger-button");
const bg = document.getElementById("hamburger-bg");
let hidden = true;

export function activateHamburger() {
  button.addEventListener("click", function () {
    hidden = !hidden;
    console.log(hidden);
    if (hidden === true) {
      hide();
    } else {
      unhide();
    }
  });
}

function unhide() {
  bg.classList.remove("hidden");
  button.innerHTML = "窱";
}

function hide() {
  bg.classList.add("hidden");
  button.innerHTML = "";
}
