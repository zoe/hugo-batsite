const checkbox = document.getElementById("darkmode-toggle");
const colorswitchers = document.getElementsByClassName("colorswitch");
const button = document.getElementById("darkmode-button-small");

function switchToLight() {
  button.innerHTML = "滛";
  for (let item of colorswitchers) {
    item.classList.add("light");
  }
}

function switchToDark() {
  button.innerHTML = "";
  for (let item of colorswitchers) {
    item.classList.remove("light");
  }
}

export function updateMode() {
  if (localStorage.getItem("light")) {
    switchToLight();
  } else {
    switchToDark();
  }
  checkbox.checked = localStorage.getItem("dark");

  button.addEventListener("click", function () {
    checkbox.click();
  });
  checkbox.addEventListener("change", function () {
    localStorage.setItem("light", this.checked);
    if (this.checked) {
      switchToLight();
    } else {
      localStorage.removeItem("light");
      switchToDark();
    }
  });
  if (localStorage.getItem("light")) {
    checkbox.checked = true;
  }
}
