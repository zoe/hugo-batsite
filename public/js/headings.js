(function addHeadingLinks() {
  var article = document.getElementById("content");
  var headings = article.querySelectorAll("h1, h2, h3, h4, h5, h6");
  headings.forEach(function (heading) {
    if (heading.id) {
      var a = document.createElement("a");
      a.innerHTML = heading.innerHTML;
      a.href = "#" + heading.id;
      heading.innerHTML = "";
      heading.appendChild(a);
    }
  });
})();
