---
title: "find me"
date: 2022-04-15T16:19:04+02:00
description: "where to find me"
draft: false
---

you can find me on a few different platforms!

[ﬧ matrix](https://matrix.to/#/@bat:matrix.kittycat.homes)
[ git](https://git.kittycat.homes/zoe)
[ bandcamp](https://zoebat.bandcamp.com/)
[ gotosocial](https://kittycat.homes/@pastelpunkbandit)
[ itch.io bat  tiger](https://bathearttiger.itch.io/)
[切 telegram](https://telegram.me/pastelpunkbandit)
[ cohost](https://cohost.org/fruitbat)
[ﭮ discord: zoebat#2042](https://discord.com/users/200666655016091648)
