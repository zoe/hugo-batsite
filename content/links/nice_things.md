---
title: "things that make me happy"
draft: false
description: "this is a list of very good things"
---

## my girlfriend!!!

[hehe fae is so cute](/links/buddies/#tess)

## nimisin generator

[this generator](https://nimisin.kittycat.homes/) comes up with valid toki pona words and descriptions for them
it's very amusing

## lipu sona pi toki pona

my girlfriend made a course to teach toki pona and now its one of the best ones out there!!!
so impressive [https://sowelitesa.kittycat.homes/lipu-sona/](https://sowelitesa.kittycat.homes/lipu-sona/)

## retrostrange tv

it's a [livestream](https://live.retrostrange.com/) of old tv shows from the 40s and 60s
the vibes are similar to those of the movies that people watch inside of other movies

## trash cat tech chat

[a really nice podcast](https://podcast.librepunk.club/tctc/) about privacy that YOU! yes you! can listen to right now!

## a whole tray of peas

she just eats the entire tray of peas!!! impressive!
the raw energy of this is just amazing {{<youtube id="HWcKcUn7e-8" class="embed">}}

## web3 is going great

very fun to see this whole thing go down
theres just so many cursed things [here](https://web3isgoinggreat.com/)

## it's probably a dog house, charlie brown

it's a silent hill / peanuts crossover zine and has a vibe i have never really experienced before
you can [download it on itch.io](https://hakobore.itch.io/its-probably-a-doghouse-charlie-brown)

{{<figure src="/img/probablyADogHouse.jpg" alt="image of charlie brown and peppermint patty walking on a fog covered street from silent hill">}}
