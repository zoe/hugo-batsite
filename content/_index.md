---
title: "home"
draft: false
description: "hello! this is my personal homepage where i show off some things i made
you can also find out some more things about me if you really want to"
menu:
  nav:
    name: ""
    weight: "1"
---

## about

### this site

the source code for [the site](https://git.kittycat.homes/zoe/hugo-batsite) and [theme](https://git.kittycat.homes/zoe/hugo-battheme) is available on [git](https://git.kittycat.homes).
this website is licensed under [the kittycat homes open source license v1](https://git.kittycat.homes/zoe/kittycat-homes-opensource-license-v1#kittycat-homes-opensource-license-v1)

### me

hi i'm zoe (she/her, fae/faer, they/them) whats up.
tech tip: you can find out more about me by looking at this website\
i'm also trans and gay and queer :)

## webring

this site is part of the [armisael webring](https://ring.bicompact.space/)!!! [please take a look](/links/buddies/#armisael-webring)!
