---
title: "supercolor"
date: 2023-03-12T15:47:55+01:00
draft: false
description: "an overengineered colorscript written in rust"
---

{{<figure src="/img/supercolor.jpg" alt="a bunch of colorful tbh creatures in my terminal">}}

this is a small but overengineered colorscript written in rust.
it's possible to install via a nix flake, or from [source](https://github.com/zoe-bat/supercolor) if flakes make you MAD and ANGRY.

theres a few builtin ascii arts, but it also supports pasing in your own.

the real purpose of this is looking pretty whenever i open a terminal, but you are allowed to pretend it's for testing your terminal colors