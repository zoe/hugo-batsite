---
title: "rssmatrix"
date: 2022-09-16T16:04:43+02:00
draft: false
description: "a circuitpython script to show rss feeds from miniflux, the weather from wtt.in and the time using aio"
---

{{<figure src="/img/rssmatrix.gif" alt="colorful scrolling text on an led matrix display, showing rss feed weather and time">}}

this is a circuitpython script for an adafruit matrix portal m4 and a 64x32 led matrix display
it can display rss feeds from miniflux, the weather from wttr.in and the time using aio

i like looking at it since it's nice and blinky but have to turn it off sometimes because it's also very distracting

finding a font for it was very weird.
first i tried the pico8 font, but that was honestly a bit too big and also used too much power for running it from a usb 2.0 port
but then i found [a website with a bunch of retro fonts and the paradise systems one just kinda spoke to me](https://int10h.org/oldschool-pc-fonts/fontlist/?3#paradise)
very nice and blocky with cool serifs

## using it yourself

- install circuitpython on your matrixportal
- clone the [git repo](https://git.kittycat.homes/zoe/rssmatrix.git)
- fill out the secrets.py file with your data
- copy the contents of the git repo onto your matrixportal
- plug in

if you want to use a different microcontroller or display u can probably figure out what to do yourself or write your own script
