---
title: "loris"
date: 2022-09-18T15:44:35+02:00
draft: false
description: "the best (soon)(i promise) fedi client for gotosocial and mastodon"
---

{{<figure src="/img/loris.jpg" alt="a screenshot of loris">}}
{{<figure src="/img/loris2.jpg" alt="a screenshot of loris">}}

loris is a native desktop client for anything that implements the mastodon client api.
it's written in dart using the flutter framework.

the goal is to have a timeline that's layed out in a similar style to tumblr, but adapted to work with fedi as well as proper multi account support

check out the newest release on [git](https://git.kittycat.homes/zoe/loris/releases)
