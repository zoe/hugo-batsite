---
title: "untitled plant game"
date: 2022-04-29T14:02:25+02:00
description: "in this game you take care of a very talkative plant"
draft: false
---
[itch.io](https://bathearttiger.itch.io/untitled-plant-game) [git](https://git.kittycat.homes/BatHeartTiger/untitled-plant-game)

there's not really a lot to do here, but you get to watch a randomly generated plant grow and talk to it!
i made this game together with [tess](/links/buddies/#tess) for the [procjam 21](https://www.procjam.com/)
{{<figure src="/img/plantgame.webp" alt="plantgame screenshot, there's a randomly generated plant in front of a window, theres also a dialogue box where the plant is talking to you!">}}

we made it in godot and it was a very pleasent experience you can play it in the browser on [itch.io](https://bathearttiger.itch.io/untitled-plant-game)!
