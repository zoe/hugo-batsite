---
title: "mystery creature"
date: 2023-03-12T15:31:26+01:00
description: "short visual novel for gameboy and web"
draft: false
---

{{<figure src="/img/creature.gif" alt="a gif with some gameplay. there are some text boxes and a cat sitting in a bright room. dialogue: there are several round items on the right. you suspect them to be a bed, food and water. \> try the food. the small creature makes a noise. it sounds displased with you. the food doesnt taste great either. overall not a good decision">}}

Mystery creature is a small visual novel for the gameboy (dmg) and [web](https://fruitsbat.itch.io/mystery-creature) in which an alien finds a cat.
It even has multiple endings! Hooray! The game was made for [a game jam](https://itch.io/jam/my-first-game-jam-winter-2023) and it was very fun to develop for gameboy. :)
