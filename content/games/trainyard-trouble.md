---
title: "trainyard trouble"
date: 2022-04-29T14:50:29+02:00
draft: false
description: "cargo management game"
---

[itch.io](https://bathearttiger.itch.io/trainyard-trouble) [git](https://git.kittycat.homes/BatHeartTiger/trainyard-trouble)

[tess](/links/buddies#tess) and i also made this game for a game jam!
you manage a cargo train terminal with three trains and your goal is to survive as long as you can without the storage area overflowing
{{<youtube class="embed" id="vBq0aBTBh8s">}}

we made this for a [godot wildjam](https://godotwildjam.com/) and it was a bit messy!
the plan was to try out godot 4, which was in alpha 4 at that point, and most of the things worked fine until we tried to export the game... the macos version had sound, but no video, the windows version had video but no sound, but the linux version worked fine so that's great!

anyways i think overall it turned out to be pretty fun once you figure out how it works :)


